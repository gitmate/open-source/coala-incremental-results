FROM coala/base:0.11
MAINTAINER Naveen Kumar Sangi nkprince007@gmail.com

RUN mkdir -p /usr/src/app
COPY . /usr/src/app

WORKDIR /usr/src/app
CMD ["python3", "run.py"]
